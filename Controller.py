#!/usr/bin/python
import RPi.GPIO as GPIO
import select
import serial
import socket
from math import atan, cos, sin, pi, sqrt, copysign
from time import sleep

# global position variables
printerX = 0
printerY = 0
# Frame values
printer_max_x = 1820
printer_max_y = 1465
# Wheel state
wheelsEnabled = False

# create socket for laser communication
serverSocket = socket.socket(family=socket.AF_INET,  type=socket.SOCK_DGRAM)
# get external_ip for laser_communication
serverIP = "192.168.43.80" #static IP used for testing
# constant, must be entered in laser software-interface
serverPort = 65432 # static port used for testing
# bind socket to given IP and port
serverSocket.bind((serverIP,serverPort))
# set socket to non-blocking
serverSocket.setblocking(False)
# define buffersize for data transmission
buffersize = 1024
# define timeout for laser connection
server_timeout = 1.5 #unit <=> seconds


def reinitSocket():
    global serverSocket
    # close old socket
    serverSocket.close()
    # create socket for laser communication
    serverSocket = socket.socket(family=socket.AF_INET,type = socket.SOCK_DGRAM)
    # bind socket to given IP and port
    serverSocket.bind((serverIP,serverPort))
    # set socket to non-blocking
    serverSocket.setblocking(False)

# Possible Wheel Commands:
#   [N]Command
#   [N]         <=> Node number, giving no node number results in command being broadcasted to all wheels

#   [N]EN       <=> Enable wheels


def enableWheels(node):
    global wheels,wheelsEnabled
    if node == -1:
        wheels.write('EN\r'.encode('ASCII'))
    else:
        wheels.write((str(node)+'EN\r').encode('ASCII'))
    wheelsEnabled = True


#   [N]V[arg]   <=> Set wheel velocity to arg (arg range= [-30k;30k])
def velocityCommand(node,velocity):
    global wheels
    if node == -1:
        wheels.write(('V'+str(velocity)+'\r').encode('ASCII'))
    else:
        wheels.write((str(node)+'V'+str(velocity)+'\r').encode('ASCII'))


#   [N]DI       <=> Disable wheels
def disableWheels(node):
    global wheels,wheelsEnabled
    if node == -1:
        wheels.write('DI\r'.encode('ASCII'))
    else:
        wheels.write((str(node)+'DI\r').encode('ASCII'))
    wheelsEnabled = False


# Wheel connection setup
wheels = serial.Serial(
    "/dev/ttyUSB0",
    baudrate = 115200,
    parity = serial.PARITY_NONE,
    stopbits = serial.STOPBITS_ONE,
    bytesize = serial.EIGHTBITS,
    writeTimeout = 0,
    timeout = 10)

# Setup for printer motors
# Constants
CW = 1          # Clockwise Rotation
CCW = 0         # Counterclockwise Rotation
delay = .0208 / 16  # Smaller delays lead to faster movement
# Pins for first motor
DIR = 21   # Direction GPIO Pin
STEP = 20  # Step GPIO Pin
MODE = (25, 8, 7)   # Microstep Resolution GPIO Pins
# Stopper-Pin for motor 1
STP1 = 6
# Pins for second motor
DIR2 = 24  # Direction Pin
STEP2 = 23 # Step Pin
MODE2 = (14,15,18)
# Stopper-Pin for motor 2
STP2 = 26

GPIO.setmode(GPIO.BCM)
# Setup for I/O motor 1
GPIO.setup(DIR, GPIO.OUT)
GPIO.setup(STEP, GPIO.OUT)
GPIO.output(DIR, CW)
GPIO.setup(STP1,GPIO.IN,pull_up_down = GPIO.PUD_UP)
# Setup for I/O motor 2
GPIO.setup(DIR2, GPIO.OUT)
GPIO.setup(STEP2, GPIO.OUT)
GPIO.output(DIR2, CW)
GPIO.setup(STP2,GPIO.IN,pull_up_down = GPIO.PUD_UP)
# Setup motor 1
GPIO.setup(MODE, GPIO.OUT)
# Setup motor 2
GPIO.setup(MODE2, GPIO.OUT)
# Smaller wheel resolutions allow smaller steps for high-precision tasks
RESOLUTION = {'Full': (0, 0, 0),
              'Half': (1, 0, 0),
              '1/4': (0, 1, 0),
              '1/8': (1, 1, 0),
              '1/16': (0, 0, 1),
              '1/32': (1, 0, 1)}
GPIO.output(MODE, RESOLUTION['1/32'])
GPIO.output(MODE2, RESOLUTION['1/32'])
# contains definitions for physical robot parameters, as they aren't needed elsewhere


def setWheelVel(xVel,yVel,omega):
    if not wheelsEnabled:
        enableWheels(-1)
    robotRadius = 1 # cm
    # velocity for wheel 0:
    vel0 = -xVel + robotRadius * omega
    vel1 = yVel * cos(pi/6) + xVel * cos(pi/3) + robotRadius * omega
    vel2 = -1 * yVel * cos(pi/6) + xVel * cos(pi/3) + robotRadius * omega
    # send command to motors
    velocityCommand(0,int(vel0))
    velocityCommand(1,int(vel1))
    velocityCommand(2,int(vel2))


# stop all wheels, wheels stay enabled
def stopWheels():
    setWheelVel(0,0,0)


# Checks the stop-switch for motor 1 on the printer
# Checks if the button is pressed for a minimum amount of time for stability
def checkSwitch1():
    for x in range(6):
        if GPIO.input(STP1) == GPIO.LOW:
            return False
        sleep(0.001)
    return True


# Checks the stop-switch for motor 2 on the printer
# Checks if the button is pressed for a minimum amount of time for stability
def checkSwitch2():
    for x in range(6):
        if GPIO.input(STP2) == GPIO.LOW:
            return False
        sleep(0.001)
    return True


# moves pen size steps in the given direction along the printers x-axis, one step equals ~0.13mm
# size must be an integer
# direction == CW => neg. X
# direction == CCW => pos. X
def xstep(size, direction):
    global printerX
    if direction == CCW and printerX + size > printer_max_x:
        xstep(printer_max_x - printerX, direction)
        print("Max reached in X direction")
        printerX = printer_max_x
        return
    elif direction == CW and printerX - size < 0:
        xstep(printerX, direction)
        print("Max reached in neg. X direction")
        printerX = 0
        return
    GPIO.output(DIR2, direction)
    for x in range(size):
        GPIO.output(STEP2, GPIO.HIGH)
        sleep(delay)
        GPIO.output(STEP2, GPIO.LOW)
        sleep(delay)
    if direction == CW:
        printerX -= size
    else:
        printerX += size


# moves pen size steps in the given direction along the printers y-axis, one step equals ~0.13mm
# size must be an integer
# direction == CCW => neg. X
# direction == CW => pos. X
def ystep(size, direction):
    global printerY
    if direction == CW and printerY + size > printer_max_y:
        ystep(printer_max_y - printerY, direction)
        print("Max reached in Y direction")
        printerY = printer_max_y
        return
    elif direction == CCW and printerY - size < 0:
        ystep(printerY, direction)
        print("Max reached in neg. Y direction")
        printerY = 0
        return
    GPIO.output(DIR, direction)
    for x in range(size):
        GPIO.output(STEP, GPIO.HIGH)
        sleep(delay)
        GPIO.output(STEP, GPIO.LOW)
        sleep(delay)
    if direction == CCW:
        printerY -= size
    else:
        printerY += size


# moves printer to zero-position
def goZero():
    global printerX
    global printerY
    GPIO.output(DIR, CCW)
    GPIO.output(DIR2, CW)
    while not checkSwitch1():
        GPIO.output(STEP, GPIO.HIGH)
        sleep(delay)
        GPIO.output(STEP, GPIO.LOW)
        sleep(delay)
    while not checkSwitch2():
        GPIO.output(STEP2, GPIO.HIGH)
        sleep(delay)
        GPIO.output(STEP2, GPIO.LOW)
        sleep(delay)
    printerX = 0
    printerY = 0


# switches printer direction
def switchPrinterDirection(current_dir):
    if current_dir == CCW:
        return CW
    else:
        return CCW



# parse incoming data from laser
# hardcoded for data format given by laser, check "output.txt" for samples
def parseLaserData(data):
    data = data[0:len(data) - 1].decode('ASCII')
    str_list = data.split(',')
    x = float(str_list[1])
    y = float(str_list[3])
    z = float(str_list[5])
    return x, y, z


# get data from server, returns x and z distance to target point as 2-dimensional vector (x,z)
def getLaserData():
    # In order to get the newest position available, the socket has to be reinitialized every time data is needed
    reinitSocket()
    # Test if a connection to the laser is established
    if testConnection():
        (x,y,z) = parseLaserData(serverSocket.recv(buffersize))
        return x, z
    # If not, handle the connection loss and try again
    else:
        handleConnectionLoss()
        return getLaserData()


def testConnection():
    # Test if data is received within the specified time
    ready_objects = select.select([serverSocket],[],[],server_timeout)
    # The server socket wasn't selected, therefore it timed out
    if len(ready_objects[0]) == 0:
        return False
    # Data was received
    else:
        return True


# In case the connection is lost, spin until connection is reestablished
# If this doesn't work, wait for interference by operator
def handleConnectionLoss():
    # ~30 degree left rotation -> check -> ~30 degree right rotation -> check
    velocity = 50
    time = 2
    while True:
        setWheelVel(0,0,velocity)
        sleep(time)
        if testConnection():
            break
        else:
            # make a longer rotation in the opposite direction
            velocity *= -1
            time *= 2
            # Once the rotation reaches a certain limit, wait for outside interference
            if time > 8:
                while not testConnection():
                    sleep(10)
                break


# Calculate the degree of rotation between the robot frame and the target frame
# Also returns the boolean "inverse_solution" to see if following calculations have to be 
# adjusted to the ambiguity of the atan function
def calcBeta():
    goZero()
    # assign measured coordinates
    m_x, m_z = getLaserData()
    # move x-line
    xstep(printer_max_x / 2, CCW)
    # get new coordinates
    new_x, new_z = getLaserData()
    # determine if the robots x-axis is within the second or third quadrant of the target frame,
    # which results in the wrong angle being computed by atan, therefore we set the boolean
    # "inverse solution". We determine this by checking the sign of deltaX
    if((new_x-m_x) >= 0):
        inverse_solution = False
    else:
        inverse_solution = True
    # calculate beta, angle between robot and target frame
    #to compensate for bad simulation resolution, we handle the error here
    if (new_x-m_x) == 0:
        new_x += 0.001
    beta = atan((new_z-m_z)/(new_x-m_x))
    # return the calculated angle the according boolean
    return beta, inverse_solution


# Given vector (x,z) in target frame pointing from target to robot,
# As well as a boolean stating if we got the correct angle,
# calculate vector (x_R,z_R) in robot frame pointing from robot to target
def transform(x,z,beta,inverse_solution):
    # ToDo: the signs used here don't exactly align with the ones we get from theory, an error in the kinematics may lead to unforeseen problems later
    x *= -1
    z *= -1
    x_R = 1*(cos(beta) * x + sin(beta)*z)
    z_R = 1*(-sin(beta) * x + cos(beta)*z)
    # Wrong beta results in the wrong signs, therefore we adjust them
    if inverse_solution:
        x_R *= -1
        z_R *= -1
    return x_R, z_R


def step(x,z,distance):
    ratio = abs(x/z)
    # make sure wheels are enabled
    if not wheelsEnabled:
        enableWheels(-1)
    # step size is determined by speed, which is chosen according to distance
    # each step activates the wheels for 3 seconds
    if distance > 2000:
        velocity = 200
    elif distance > 1000:
        velocity = 150
    elif distance > 500:
        velocity = 100
    else:
        velocity = 50
    # drive wheels in the direction of the target
    # copysign(1,_) ensures the sign of the specified velocity is correct
    setWheelVel(int(velocity * ratio * copysign(1,x)),int(velocity*copysign(1,z)),0)
    sleep(3)
    # stop wheels
    setWheelVel(0,0,0)


# Calculate distance with the pythagorean theorem
def calcDistance(x,y):
    return sqrt(x**2 + y**2)


# Initialize all components
def init():
    goZero()
    enableWheels(-1)


# Prepare all components for shutdown
def cleanUp():
    global serverSocket, wheelsEnabled
    GPIO.cleanup()
    serverSocket.close()
    disableWheels(-1)


# Perform task
def run():
    # prepare for execution
    init()
    x_init, y_init = getLaserData()
    distance = calcDistance(x_init,y_init)
    # Calculate degree between robot and target frame
    beta = 1
    # Use wheels until distance < 10cm
    while distance > 100:
        beta, inverse_solution = calcBeta()
        # Get position of robot in target frame
        x_pos,z_pos = getLaserData()
        # Transform target coordinates in robot frame
        x_dif, z_dif = transform(x_pos,z_pos,beta, inverse_solution)
        # Calculate the distance to the target
        distance = calcDistance(x_dif,z_dif)
        # Calculate the ratio of delta x to delta z
        ratio = abs(x_dif/z_dif)
        # Take a step in the direction of the target
        step(x_dif,z_dif,distance)
        # Update position
        x_pos, z_pos = getLaserData()
        x_dif, z_dif = transform(x_pos, z_pos, beta, inverse_solution)
        distance = calcDistance(x_dif,z_dif)
        # Check if the ratio has remained reasonably equal, meaning the course is still accurate
        newRatio = abs(x_dif/z_dif)
        # If the ratio has changed beyond 10%, beta is recalculated
        if newRatio/ratio <= 0.9 or newRatio >= 1.1:
            beta, inverse_solution = calcBeta()
        # If not, the current course can be maintained
        else:
            step(x_dif,z_dif,distance)
    # Robot in close proximity of target, wheels can be disabled
    disableWheels(-1)
    # Get position of robot in target frame
    x_pos, z_pos = getLaserData()
    # Calculate the distance to the target
    distance = calcDistance(x_pos, z_pos)
    # Since the wheels are disabled now, and the precision for beta is unreliable on a scale this small,
    # we use another algorithm/another set of helper methods for minimizing the distance
    # First, move motors to zero position, then iterate over all x values until distance is minimized
    # then the same is done for the y axis
    # To achieve high precision, step size is gradually decreased
    stepsize = 500
    direction = CCW
    goZero()
    # Readjust printer in x direction
    while True:
        # take a step in the direction of the target
        xstep(stepsize,direction)
        # calculate new distance
        x_pos, z_pos = getLaserData()
        newDistance = calcDistance(x_pos,z_pos)
        # if the distance decreased, keep going in the same direction
        if newDistance < distance:
            distance = newDistance
            continue
        # if not, the target was overshot, so decrease the stepsize and change direction
        else:
            distance = newDistance
            # if the stepsize becomes small enough, we consider ourselves to be "on target" in the x dimension
            stepsize = int(stepsize/2)
            if stepsize < 5:
                break
            direction = switchPrinterDirection(direction)
    # Readjust printer in y direction
    # Reset stepsize and direction
    stepsize = 500
    direction = CW
    while True:
        # take a step in the direction of the target
        ystep(stepsize,direction)
        # calculate new distance
        x_pos, z_pos = getLaserData()
        newDistance = calcDistance(x_pos, z_pos)
        # if the distance decreased, keep going in the same direction
        if newDistance < distance:
            distance = newDistance
            continue
        # if not, the target was overshot, so decrease the stepsize and change direction
        else:
            distance = newDistance
            # if the stepsize becomes small enough, we consider ourselves to be "on target" in the x dimension
            stepsize = int(stepsize / 2)
            if stepsize < 5:
                break
    # robot has reached target position, prepare for shutdown
    cleanUp()



def demoRun():
    init()
    x, z = getLaserData()
    beta, inverse_solution = calcBeta()
    x_t, z_t = transform(x,z,beta,inverse_solution)
    print("X_t in robot frame: " + str(x_t) + "Z_t in robot frame: " + str(z_t))
    cleanUp()

def testRun():
    calcBeta()
    cleanUp()

ystep(500,CW)
sleep(5)
run()
#ystep(500,CW)
